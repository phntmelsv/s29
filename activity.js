// check difference of $in and $regex

db.users.find({
	$or:[
		{firstName: {
			$regex: "s",
			$options: "i"
		}},
		{lastName: {
			$regex: "d",
			$options: "i"
		}}
	]
},
{
	_id: 0,
	firstName: 1,
	lastName: 1
});

// show only the firstName and lastName fields, hide _id

db.users.find({
	$and:[
		{department: "HR"},
		{age: {
			$gte:70
		}}
	]
});

db.users.find({
	$and:[
		{firstName: {
			$regex: "e",
			$options: "i"
		}},
		{age: {
			$lte:30
		}}
	]
});